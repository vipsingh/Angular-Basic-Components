import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {
  chart1 = [];
  chart2 = [];
  constructor(private _weather: WeatherService) { }

  ngOnInit() {
    this._weather.dailyForecast()
      .subscribe(res => {
        const temp_max = res['list'].map(res  => res.main.temp_max);
        const temp_min = res['list'].map(res => res.main.temp_min);
        const sea_level = res['list'].map(res => res.main.sea_level);
        const grand_level = res['list'].map(res => res.main.grnd_level);
        const alldates = res['list'].map(res => res.dt);

        const weatherDates = [];
        alldates.forEach((res) => {
        const jsdate = new Date(res * 1000);
        weatherDates.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric' }));
      });

      // Charts codes started

      this.chart1 = new Chart('canvas', {
        type: 'line',
        data: {
          labels: weatherDates,
          datasets: [
            {
              label: 'Temperature-Max',
              data: temp_max,
              backgroundColor : '#3cba9f',
              borderColor: '#3cba9f',
              fill: false,
            },
            {
              label: 'Temperature-Min',
              data: temp_min,
              backgroundColor : '#ffcc00',
              borderColor: '#ffcc00',
              fill: false
            },
          ]
        },
        options: {
          legend: {
            display: true,
            fullWidth: true
          },
          scales: {
            xAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Dates',
                fontStyle: 'bold',
                fontColor: 'red'
              }
            }],
            yAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Temperature Value',
                fontStyle: 'bold',
                fontColor: 'red'
              }
            }],
          }
        }
      });

  this.chart2 = new Chart('canvas1', {
    type: 'bar',
    data: {
      labels: weatherDates,
      datasets: [
        {
          label: 'Sea Level',
          data: sea_level,
          backgroundColor : '#ec5525',
          borderColor: '#ec5525',
          fill: false
        },
        {
          label: 'Ground Level',
          data: grand_level,
          backgroundColor : '#34434e',
          borderColor: '#34434e',
          fill: false
        },
      ]
    },
    options: {
      legend: {
        display: true,
        fullWidth: true
      },
      scales: {
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Dates',
            fontStyle: 'bold',
            fontColor: 'red'
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Sea & Ground Level Value',
            fontStyle: 'bold',
            fontColor: 'red'
          }
        }],
      }
    }
  });
});
}
}
