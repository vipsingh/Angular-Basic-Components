import { Product } from './product';

export const Products: Product[] =
[
    { id: 11, name: 'Laptop', price: 60000 },
    { id: 12, name: 'Mobile', price: 40000 },
    { id: 13, name: 'Tab', price: 55000 },
    { id: 14, name: 'Clothes', price: 800 },
    { id: 15, name: 'Furniture', price: 85000 },
    { id: 16, name: 'Chair', price: 850 },
    { id: 17, name: 'Fruits', price: 100 },
    { id: 18, name: 'Flowers', price: 150 },
];
