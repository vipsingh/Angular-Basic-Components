import { Injectable } from '@angular/core';
import { Product } from './Product';
import { Products } from './Products';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() { }

  getProducts(): Product[] {
    return Products;
  }
}
