import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-structure',
  templateUrl: './structure.component.html',
  styleUrls: ['./structure.component.css']
})
export class StructureComponent implements OnInit {
  newProducts;
  isavailable = false;
  vehicle = 'car';
  months = ['January', 'Feburary', 'March', 'April',
    'May', 'June', 'July', 'August',
    'September', 'October', 'November', 'December'];

  empData = [
    {
      id: 1, name: 'ABC', age: 25,
    },
    {
      id: 2, name: 'DEF', age: 30,
    },
    {
      id: 3, name: 'GHI', age: 35,
    },
    {
      id: 4, name: 'JKL', age: 40,
    }
  ];


  constructor(private productservice: ProductService) {

   }

    getproduct(): void {
      this.newProducts = this.productservice.getProducts();
    }

  ngOnInit() {
    this.getproduct();
  }

}
