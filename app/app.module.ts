import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { BindingComponent } from './binding/binding.component';
import { StructureComponent } from './structure/structure.component';
import { PipeComponent } from './pipe/pipe.component';
import { HomeComponent } from './home/home.component';
import { ProductService } from './product.service';
import { WeatherService } from './weather.service';
import { BasicComponent } from './basic/basic.component';
import { HttpClientModule } from '@angular/common/http';
import { ChartsComponent } from './charts/charts.component';

@NgModule({
  declarations: [
    AppComponent,
    BindingComponent,
    StructureComponent,
    PipeComponent,
    HomeComponent,
    BasicComponent,
    ChartsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'binding', component: BindingComponent},
      { path: 'pipe', component: PipeComponent},
      { path: 'structure', component: StructureComponent},
      {
        path: 'structure', component: StructureComponent,
        children: [
          {
            path: 'pipe', component: PipeComponent
          }
        ]
      },
      { path: 'home', component: HomeComponent},
      { path: 'charts', component: ChartsComponent},
      { path: '', redirectTo: 'home', pathMatch: 'full'}
    ])
  ],
  providers: [ProductService, WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
