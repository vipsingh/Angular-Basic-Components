import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {

  name: string;
  myId = 'UrId';
  isDisabled = false;
  public newClass = 'text-danger';
  public hasError = false;
  public isSpecial = true;
  public messageClasses = {
    'text-primary': !this.hasError,
    'text-danger': this.hasError,
    'text-special': this.isSpecial
  };
  public highlightColor = 'orange';
  public titleStyles = {
    backgroundColor: 'grey',
    color: 'blue',
    fontStyle: 'italic'
    };

  constructor() { }
  myClickFunction(event) {
    alert('Button is clicked');
    console.log('This is event' + event);
  }
  ngOnInit() {
  }

}
