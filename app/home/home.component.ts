import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'SCTPL';
  name = 'newadmin';
  sum = 30;
  public url = window.location.href;

  greetUser() {
    return 'Hello' + this.name;
    }

    months = ['January','Feburary','March','April','May','June','July','August','September','October','November','December'];

  constructor() { }

  ngOnInit() {
  }

}
