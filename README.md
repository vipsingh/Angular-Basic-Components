# Angular-Basic-Components
Inclues all basic components - Binding,Pipes,Structure directives, charts etc.

<p>components includes:</p>
<h3>Binding</h3>
<ul>
<li>Data Binding</li>
<li>Class Binding</li>
<li>Style Binding</li>
<li>Property Binding</li>
<li>Event Binding</li>
<li>Two way Binding</li>
</ul>
<hr>
<h3>Structure Directives</h3>
<ul>
<li>ngIf</li>
<li>ngFor</li>
<li>ngSwitch</li>
</ul>
<hr>
<h3>Pipes</h3>
<ul>
<li>date</li>
<li>currency</li>
<li>number</li>
<li>slice</li>
<li>json</li>  
</ul>
<hr>
<h3>Charts</h3>
<ul>
<li>Line Chart</li>
<li>Bar Chart</li>
</ul>  
<hr>
